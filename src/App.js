import { BrowserRouter, Route, Routes } from "react-router-dom";
import Carla from "./pages/Carla";
import Home from "./pages/Home";
import "./index.css";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/carla" element={<Carla />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
