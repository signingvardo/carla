import React from "react";

const Articles = ({ className, image, titre, title }) => {
  return (
    <div
      className={` px-3 pt-6 pb-1  rounded-xl xs:w-[153px]  bg-primary ${
        className ? className : ""
      }`}
    >
      <div className=" flex justify-center">
        <img src={image} alt="" />
      </div>
      <div className=" font-semibold text-lg text-left text-white">
        <p>{titre} </p>
        <p>{title} </p>
      </div>
    </div>
  );
};

export default Articles;
