import React from "react";
import suivant from "../images/suivant.svg";
const Button = () => {
  return (
    <div className=" bg-blue-800">
      <img src={suivant} alt="" />
    </div>
  );
};

export default Button;
