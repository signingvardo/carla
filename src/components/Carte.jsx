import React from "react";
const Carte = ({ image, titre, phrase, img }) => {
  return (
    <div className=" bg-white rounded-md shadow-md flex flex-col px-3 py-5 items-center justify-center  w-[309px] ">
      <div className=" relative">
        <img src={img} alt="" className=" absolute top-0 -left-16" />
        <img src={image} alt="" />
      </div>
      <div>
        <h2 className=" font-bold text-3xl text-black">{titre}</h2>
        <p className=" font-normal text-[22px] text-black">{phrase}</p>
      </div>
    </div>
  );
};

export default Carte;
