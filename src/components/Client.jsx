import React from "react";

const Client = () => {
  return (
    <div className=" w-full mt-20">
      <h2 className=" font-bold text-center mb-10 text-[40px] text-[#1C1A0F]">
        Conçu pour vous et vos clients
      </h2>
      <div className="bg-[#FCCD00] relative px-8 lg:px-32 pb-8 pt-4 w-full h-auto">
        <div className=" h-[97px] w-[97px] lg:h-[200px] lg:w-[222px] absolute left-0 -top-6 lg:-top-14 bg-[#5DDBF7]/30"></div>
        <div className=" lg:flex lg:justify-between">
          <div className=" bg-white w-full h-auto lg:w-[49%] text-center px-3 lg:relative z-10 py-[18px] lg:px-10 lg:py-7">
            Avec Carla+ Curls, vous disposez de l'occasion d'aller à la
            rencontre de milliers de clients qui sont à la recherche de services
            pour leurs soins capillaires. Nous vous permettons également de
            garder contact et de fidéliser vos clients une fois le service
            terminé. <br /> Nous avons développer une solution digitale de
            pointe avec les meilleurs ouils afin de développer votre activité.
          </div>
          <div className="bg-white w-full lg:w-[49%] h-[218px] mt-5 lg:mt-0"></div>
        </div>
        <div className=" w-full lg:w-[50%] flex justify-center mt-5">
          <button className=" h-[52px] w-[374px] bg-[#1C1A0F] text-white font-bold text-2xl">
            Devenir partenaire
          </button>
        </div>
      </div>
    </div>
  );
};

export default Client;
