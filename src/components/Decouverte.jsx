import React from "react";
import head from "../images/head.svg";
const Decouverte = () => {
  return (
    <div className=" ">
      <img src={head} className=" w-full relative" alt="" />
      <div className="absolute top-16 left-8 max-w-4xl lg:left-60  lg:top-56">
        <h2 className=" text-white text-[22px] md:text-[40px] font-bold mb-2 lg:mb-8">
          Découvrez Carla+ Curls
        </h2>
        <p className=" text-white text-[16px] md:text-2xl font-normal mb-5 md:mb-11 lg:mb-20 ">
          Le premier outil qui utilise l’intelligence artificielle pour
          encourager la communauté des cheveux bouclés et frisés à valoriser
          leurs cheveux naturels
        </p>
        <button className=" h-[52px] w-[236px] bg-[#FCCC01] text-noir font-bold text-2xl">
          En savoir plus
        </button>
      </div>
    </div>
  );
};

export default Decouverte;
