import React from "react";
import phone from "../images/phone.svg";
const Download = () => {
  return (
    <div className="mb-32 relative flex justify-between items-center flex-col md:flex-row md:items-start pl-8 ">
      <div className=" ">
        <h2 className=" font-bold text-4xl">Téléchargez l’app !</h2>
        <p className=" text-[22px] md:w-[279px] lg:w-auto my-8">
          Réservez vos soins beauté et traitements de cheveux directement sur
          l’app Carla+ Curls.
        </p>
        <button className=" w-[322px] h-[52px] bg-[#FCCC01] cursor-pointer text-white font-bold text-[25px] ">
          Téléchargez
        </button>
      </div>
      <div className=" sm:w-[450px] lg:w-[570px] relative">
        <div className="hidden md:block bg-[#FCCC01]/50 absolute top-16  rounded-full w-24 h-24"></div>
        <img
          src={phone}
          alt=""
          className="absolute xs:h-[280px] xs:w-[248px] md:h-[380px] md:w-[348px] md:left-20 z-10 "
        />
        <div className=" absolute md:top-16 xs:top-8 md:-right-6 xs:right-0  rounded-full bg-[#5BDAF5]/50 md:w-[217px] md:h-[217px] xs:w-[200px] xs:h-[200px]"></div>
      </div>
    </div>
  );
};

export default Download;
