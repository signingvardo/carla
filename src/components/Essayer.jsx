import React from "react";
import photo from "../images/photo.svg";
import cote from "../images/cote.svg";
const Essayer = () => {
  return (
    <div className=" mb-10 lg:flex flex-col items-center justify-between">
      <h2 className=" text-black font-bold text-center text-[40px] mb-10">
        Ils ont essayé Carla+
      </h2>
      <div className=" flex flex-col items-center md:flex-row px-9 justify-between">
        <div className="mb-5 md:mb-0">
          <div className=" h-44 w-44 rounded-full overflow-hidden">
            <img src={photo} alt="" />
          </div>
          <p className=" font-bold text-4xl text-noir">Armelita</p>
          <p className=" text-2xl text-noir ">Rad FM 89.3</p>
        </div>
        <div className=" lg:ml-60">
          <div className=" relative px-7 py-9 text-center bg-[#5DDBF7] h-52 md:w-[439px] ">
            <img src={cote} alt="" className=" absolute -top-3 -left-3" />
            <p className=" font-bold text-xl text-white mb-6">Innovant !</p>
            <p className=" font-bold text-xl text-white mb-6">
              Grâce à carla+, je sais enfin quoi faire pour avoir des cheveux en
              santé mes enfants et moi.
            </p>
            <img
              src={cote}
              alt=""
              className=" rotate-180 absolute -bottom-3 -right-3"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Essayer;
