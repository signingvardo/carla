import React from "react";
import logo from "../images/logo.svg";
import menu from "../images/menu.svg";
const Header = () => {
  return (
    <div className=" overflow-x-hidden">
      <div className=" bg-[#2EBDDC] w-screen overflow-x-auto px-8 h-14 flex items-center justify-between">
        <img src={logo} alt="" />
        <img src={menu} alt="" />
      </div>
    </div>
  );
};

export default Header;
