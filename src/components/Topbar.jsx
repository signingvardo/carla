import React from "react";
import c from "../images/icon.svg";
const Topbar = () => {
  return (
    <div className=" flex bg-white/50 h-[99px] ">
      <button className=" text-white  text-2xl mr-1  w-full font-bold bg-primary h-14">
        Réserver
      </button>
      <button className=" flex justify-center items-center mr-1  font-bold w-full bg-secondary h-14">
        <img src={c} alt="" />
      </button>
      <button className=" text-white w-full text-2xl font-bold bg-primary h-14">
        E-shop
      </button>
    </div>
  );
};

export default Topbar;
