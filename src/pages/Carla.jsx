import React from "react";
import Articles from "../components/Articles";
import Topbar from "../components/Topbar";
import cal from "../images/calenda.svg";
import logo from "../images/logo2.svg";
import discover from "../images/discover.svg";
import stop from "../images/stop.svg";
import caution from "../images/caution.svg";
const Carla = () => {
  return (
    <div className="  flex items-center justify-center">
      <div className="lg:w-[50%]">
        <div className=" px-8 pt-6 mb-[100px]">
          <div>
            <div className=" flex justify-between w-full items-center">
              <svg
                width="20"
                height="15"
                viewBox="0 0 20 15"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M0 1C0 0.734784 0.105357 0.48043 0.292893 0.292893C0.48043 0.105357 0.734784 0 1 0H19C19.2652 0 19.5196 0.105357 19.7071 0.292893C19.8946 0.48043 20 0.734784 20 1C20 1.26522 19.8946 1.51957 19.7071 1.70711C19.5196 1.89464 19.2652 2 19 2H1C0.734784 2 0.48043 1.89464 0.292893 1.70711C0.105357 1.51957 0 1.26522 0 1Z"
                  fill="#FCCC01"
                />
                <path
                  d="M0 7.03198C0 6.76677 0.105357 6.51241 0.292893 6.32488C0.48043 6.13734 0.734784 6.03198 1 6.03198H19C19.2652 6.03198 19.5196 6.13734 19.7071 6.32488C19.8946 6.51241 20 6.76677 20 7.03198C20 7.2972 19.8946 7.55155 19.7071 7.73909C19.5196 7.92663 19.2652 8.03198 19 8.03198H1C0.734784 8.03198 0.48043 7.92663 0.292893 7.73909C0.105357 7.55155 0 7.2972 0 7.03198Z"
                  fill="#FCCC01"
                />
                <path
                  d="M1 12.064C0.734784 12.064 0.48043 12.1693 0.292893 12.3569C0.105357 12.5444 0 12.7987 0 13.064C0 13.3292 0.105357 13.5835 0.292893 13.7711C0.48043 13.9586 0.734784 14.064 1 14.064H19C19.2652 14.064 19.5196 13.9586 19.7071 13.7711C19.8946 13.5835 20 13.3292 20 13.064C20 12.7987 19.8946 12.5444 19.7071 12.3569C19.5196 12.1693 19.2652 12.064 19 12.064H1Z"
                  fill="#FCCC01"
                />
              </svg>
              <img src={cal} alt="" />
            </div>
            <div className="mt-3 flex justify-center">
              <img src={logo} alt="" />
            </div>
          </div>
          <div className=" mt-4">
            <h2 className=" text-primary text-2xl font-bold">
              Hey Alphonsine,
            </h2>
            <p className=" text-[22px]">Nous sommes le 11 Février 2022.</p>
          </div>
          <div className=" mt-4">
            <h2 className=" text-secondary text-2xl font-bold">
              À faire aujourd’hui
            </h2>
            <div className="flex items-center">
              <input
                type="checkbox"
                name=""
                id=""
                className=" border-secondary h-4 w-4"
              />
              <p className=" text-[22px] ml-3">Shampoing</p>
            </div>
          </div>
          <div className=" mt-4">
            <h2 className=" text-secondary text-2xl font-bold">
              Message de votre salon
            </h2>
            <p className=" text-[22px]">
              Allô Lucie, toute l’équipe Glam Coiffure te souhaite un joyeux
              anniversaire. Pour l’occasion, reçois 20% de rabais sur ta
              prochaine réservation.
            </p>
          </div>
          <div className=" mt-4 bg-[#F55B5B] rounded-xl px-2 flex flex-col items-center justify-center py-3 w-full">
            <h2 className=" text-lg text-white font-bold mb-2">
              PROMOTION 1ÈRE COMMANDE
            </h2>
            <p className=" text-[22px] text-center text-white">
              Un bon d’achat offert après votre 1ère commande sur l’E-shop
              Carla+
            </p>
          </div>
          <div className=" mt-5 ">
            <h2 className=" text-primary text-2xl font-bold mb-3">Articles</h2>
            <div className=" flex justify-between md:px-40  ">
              <div className="mr-2">
                <Articles
                  title={"Carla+Curls"}
                  titre={"Decouvrir"}
                  image={discover}
                />
              </div>
              <Articles
                titre={"Eviter le"}
                title={"defrisage"}
                image={stop}
                className=" bg-secondary"
              />
            </div>
            <Articles
              titre={"Attention"}
              title={"Les produits nocifs pour les cheveux"}
              image={caution}
              className=" !w-full mt-3 !bg-noir"
            />
          </div>
        </div>
        <div className="px-1 fixed bottom-0 w-full lg:w-[50%]">
          <Topbar />
        </div>
      </div>
    </div>
  );
};

export default Carla;
