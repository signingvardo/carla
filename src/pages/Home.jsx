import React, { useState } from "react";
import Carte from "../components/Carte";
import Client from "../components/Client";
import Decouverte from "../components/Decouverte";
import Download from "../components/Download";
import Essayer from "../components/Essayer";
import Header from "../components/Header";
import { Swiper, SwiperSlide, useSwiper } from "swiper/react";
import suivant from "../images/suivant.svg";
import avartar from "../images/avartar.svg";
import hello from "../images/hello.svg";
import noti from "../images/agenda.svg";
import pc from "../images/pc.svg";
import "swiper/css";
import "./home.css";
import { Mousewheel, Keyboard } from "swiper";

const Home = () => {
  const [currentIndex, setCurrentIndex] = useState(0);

  return (
    <div className=" overflow-x-hidden">
      <Header />
      <Decouverte />
      <div className="">
        <div className=" px-8 lg:px-60 mt-20 flex justify-between items-center">
          <>
            <Swiper
              defaultValue={0}
              onSlideChange={(e) => {
                setCurrentIndex(e.realIndex);
              }}
              breakpoints={{
                640: {
                  slidesPerView: 1,
                  spaceBetween: 20,
                },
                768: {
                  slidesPerView: 2,
                  spaceBetween: 20,
                },
                1024: {
                  slidesPerView: 3,
                  spaceBetween: 20,
                },
              }}
              keyboard={true}
              modules={[Mousewheel, Keyboard]}
              className=" relative "
            >
              <div className=" lg:hidden flex justify-between w-full absolute top-[50%] z-10  ">
                <LeftArrow />
                <RightArrow />
              </div>
              <div className=" lg:hidden  flex justify-center w-full">
                <Pagination currentIndex={currentIndex} />
              </div>
              <SwiperSlide>
                <Carte
                  image={avartar}
                  img={hello}
                  titre={"Assistant personnel"}
                  phrase={
                    "  Carla+ Curls est la première application de soins qui offre des conseils personnalisés grâce à une intelligence artificielle. Ne vous inquietez plus, Carla+ Curls a toutes les réponses pour vous !"
                  }
                />
              </SwiperSlide>
              <SwiperSlide>
                <Carte
                  image={noti}
                  titre={"Agenda dynamique"}
                  phrase={
                    "Carla+ Curls tient à jour votre agenda pour tous vos rendez-vous capillaires, soins, renouvellements de coupes, etc... et vous envoie des notifications de rappel. Vous n’avez même plus besoin de vous souvenir !"
                  }
                />
              </SwiperSlide>
              <SwiperSlide>
                <Carte
                  image={pc}
                  titre={"Agenda dynamique"}
                  phrase={
                    "Carla+ Curls tient à jour votre agenda pour tous vos rendez-vous capillaires, soins, renouvellements de coupes, etc... et vous envoie des notifications de rappel. Vous n’avez même plus besoin de vous souvenir !"
                  }
                />
              </SwiperSlide>
            </Swiper>
          </>
        </div>
        <div className=" lg:pl-56 h-[400px] mt-32 mb-32">
          <Download />
        </div>
        <div className=" mt-32">
          <Client />
        </div>
        <div className=" mt-32">
          <Essayer />
        </div>
      </div>
    </div>
  );
};

export default Home;
const LeftArrow = () => {
  const swiper = useSwiper();

  return (
    <img
      src={suivant}
      alt=""
      className="rotate-180  "
      onClick={() => {
        swiper.slidePrev();
      }}
    />
  );
};

const RightArrow = () => {
  const swiper = useSwiper();

  return (
    <div>
      <img
        src={suivant}
        alt=""
        onClick={() => {
          swiper.slideNext();
        }}
      />
    </div>
  );
};
const Pagination = ({ currentIndex }) => {
  return (
    <div className=" flex">
      {Array(3)
        .fill(0)
        .map((e, index) => {
          return (
            <div
              key={index}
              className={` mr-4 rounded-full flex transition-all duration-200 ${
                index === currentIndex
                  ? "w-[49px] h-[8px] bg-primary"
                  : "w-[10px] h-[10px] bg-[#E3E3E3]"
              }`}
            ></div>
          );
        })}
    </div>
  );
};
