/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    container: {
      padding: {
        DEFAULT: "30px",
        lg: "0",
      },
    },
    screens: {
      xs: "0px",
      sm: "414px",
      md: "768px",
      lg: "1024px",
      xl: "1440px",
    },
    extend: {
      colors: {
        primary: "#FCCC01",
        secondary: "#5BDAF5",
        noir: "#1C1A0F",
      },
    },
  },
  plugins: [],
};
